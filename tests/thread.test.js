const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const Thread = require('../db/threadModel');
const User = require('../db/userModel');

chai.use(chaiHttp);
chai.should();

let testThread = {
    id: null,
    username: null,
    title: 'A new thread title',
    content: 'This is the thread content.'
};

describe('thread.js tests', _ => {

    // Setup test database
    before(done => {
        Thread.deleteMany({}).then(_ => {
            User.deleteMany({}).then(_ => {
                chai.request(app).post('/user').send({
                    username: 'test',
                    password: 'test'
                }).end((err, res) => {
                    if (err) throw err;
                    if(res.body.id){
                        testThread.username = 'test';
                        done();
                    }
                });
            });
        });
    });

    // Clean-up database
    after(done => {
        User.deleteMany({}).then(_ => {
            done();
        });
    });

    it('POST /thread - should create a new thread', done => {
        chai.request(app).post('/thread').send(testThread).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('id');
            testThread.id = res.body.id;
            done();
        });
    });

    it('POST /thread - should not create a new thread', done => {
        chai.request(app).post('/thread').send({
            username: testThread.username,
            content: 'This is a thread without a title, so it should error.'
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('GET /thread/:id - should return the newly created thread', done => {
        chai.request(app).get(`/thread/${testThread.id}`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            res.body.should.have.property('author');
            res.body.should.have.property('_id').eql(testThread.id);
            res.body.should.have.property('title').eql(testThread.title);
            res.body.should.have.property('content').eql(testThread.content);
            res.body.author.should.have.property('username').eql(testThread.username);
            done();
        })
    });

    it('GET /thread - should return a list of threads', done => {
        chai.request(app).get('/thread').end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            res.body.should.be.a('array');
            const thread = res.body[0]; // latest added thread from previous test
            thread.should.have.property('_id').eql(testThread.id);
            thread.should.have.property('title').eql(testThread.title);
            thread.should.have.property('content').eql(testThread.content);
            thread.should.have.property('username').eql(testThread.username);
            done();
        });
    });

    it('GET /thread/sort/upvotes - should return a list of threads based on upvotes', done => {
        chai.request(app).get('/thread/sort/upvotes').end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            res.body.should.be.a('array');
            const thread = res.body[0]; // latest added thread from previous test
            thread.should.have.property('_id').eql(testThread.id);
            thread.should.have.property('title').eql(testThread.title);
            thread.should.have.property('content').eql(testThread.content);
            done();
        });
    });

    it('PATCH /thread/content/:id - should edit content', done => {
        chai.request(app).patch(`/thread/content/${testThread.id}`).send({
            content: 'Updated content'
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });

    it('PATCH /thread/content/:id - should not edit content, missing required parameter', done => {
        chai.request(app).patch(`/thread/content/${testThread.id}`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('POST /thread/:id/upvote - should upvote thread', done => {
        chai.request(app).post(`/thread/${testThread.id}/upvote`).send({
            username: testThread.username
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });

    it('POST /thread/:id/upvote - should not upvote thread, missing required parameter', done => {
        chai.request(app).post(`/thread/${testThread.id}/upvote`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('POST /thread/:id/downvote - should downvote thread', done => {
        chai.request(app).post(`/thread/${testThread.id}/downvote`).send({
            username: testThread.username
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });

    it('POST /thread/:id/downvote - should not downvote thread, missing required parameter', done => {
        chai.request(app).post(`/thread/${testThread.id}/downvote`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('DELETE /thread/:id - should delete thread', done => {
        chai.request(app).delete(`/thread/${testThread.id}`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });
});