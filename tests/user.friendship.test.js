const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const User = require('../db/userModel');
const neo4j = require('../db/neo4j');

chai.use(chaiHttp);
chai.should();

let username1;
let username2;

describe('user.friendship.js tests', _ => {
    // Setup test database
    before(done => {
        const session = neo4j.session();
        const result = session.run('MATCH (n) DETACH DELETE n');

        result.then(_ => {
            User.deleteMany({}).then(_ => {
                chai.request(app).post('/user').send({
                    username: 'test',
                    password: 'test'
                }).end((err, res) => {
                    if (err) throw err;
                    if(res.body.id){
                        username1 = 'test';
                    }
                    chai.request(app).post('/user').send({
                        username: 'test2',
                        password: 'test2'
                    }).end((err, res) => {
                        if (err) throw err;
                        if(res.body.id){
                            username2 = 'test2';
                            done();
                        }
                    });
                });
            });
        });
    });

    // Clean-up database
    after(done => {
        const session = neo4j.session();
        const result = session.run('MATCH (n) DETACH DELETE n');
        result.then(_ => {
            User.deleteMany({}).then(_ => {
                done();
            });
        });
    });

    it('POST /user/friends - should create a new friendship', done => {
        chai.request(app).post('/user/friends').send({
            username1: username1,
            username2: username2
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });

    it('POST /user/friends - should not create a new friendship', done => {
        chai.request(app).post('/user/friends').send({
            username1: username1
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('DELETE /user/friends - should not delete the friendship', done => {
        chai.request(app).delete('/user/friends').send({
            username1: username1
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('DELETE /user/friends - should delete the friendship', done => {
        chai.request(app).delete('/user/friends').send({
            username1: username1,
            username2: username2
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });
});