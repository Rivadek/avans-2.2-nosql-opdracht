const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const app = require('../app');
const mongoose = require('mongoose');
const Thread = require('../db/threadModel');
const User = require('../db/userModel');
const Comment = require('../db/commentModel');

chai.use(chaiHttp);

let testUser = {
    id: null,
    username: 'ricardo',
    password: 'ricardo123'
};

let testUser2 = {
    id: null,
    username: 'kevin',
    password: 'kevin123'
};

describe('user.js tests', _ => {

    // Setup test database
    before(done => {
        User.deleteMany({}).then(_ => {
            chai.request(app).post('/user').send({
                username: testUser2.username,
                password: testUser2.password
            }).end((err, res) => {
                if (err) throw err;
                if(res.body.id){
                    testUser2.id = res.body.id;
                    done();
                }
            });
        });
    });

    // Clean-up database
    after(done => {
        User.deleteMany({}).then(done());
    });

    it('POST /user - should create a new user', done => {
        chai.request(app).post(`/user`).send(testUser).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('id');
            testUser.id = res.body.id;
            done();
        });
    });

    it('POST /user - should not create a new user: Missing parameters', done => {
        chai.request(app).post(`/user`).send({ }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('PATCH /user/password - should edit users password', done => {
        chai.request(app).patch(`/user/password`).send({
            username: testUser.username,
            password: testUser.password,
            newpassword: 'newPassword123'
        }).end((err, res) => {
            if (err) throw err;
            testUser.password = 'newPassword123';
            res.should.have.status(200);
            done();
        });
    });

    it('PATCH /user/password - should not edit password, password provided is incorrect', done => {
        chai.request(app).patch(`/user/password`).send({
            username: testUser.username,
            password: 'ricardo123',
            newpassword: 'thisShouldError'
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(401);
            done();
        });
    });

    it('DELETE /user - should delete user', done => {
        chai.request(app).delete(`/user`).send({
            username: testUser2.username,
            password: testUser2.password
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });

    it('DELETE /user - should not delete user: Password incorrect', done => {
        chai.request(app).delete(`/user`).send({
            username: testUser.username,
            password: 'ThisShouldError'
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(401);
            done();
        });
    });
});