const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const Thread = require('../db/threadModel');
const User = require('../db/userModel');
const Comment = require('../db/commentModel');

chai.use(chaiHttp);
chai.should();

// beforeEach create user
let testThread = {
    id: null,
    username: null,
    title: 'A new thread title',
    content: 'This is the thread content.'
};

let testComment = {
    id: null,
    author: null,
    content: 'Comment content'
};

describe('comment.js tests', _ => {

    // Setup test database
    before(done => {
        Comment.deleteMany({}).then(_ => {
            User.deleteMany({}).then(_ => {
                chai.request(app).post('/user').send({
                    username: 'test',
                    password: 'test'
                }).then(( res) => {
                    //if (err) throw err;
                    if(res.body.id){
                        testComment.author = 'test';
                        testThread.username = 'test';
                        chai.request(app).post('/thread').send(testThread).end( (err, res) =>{
                            if (err) throw err;
                            if(res.body.id) testThread.id = res.body.id
                            done();
                        })
                        //done();
                    }
                }).catch( err => { console.error( err )});
            });
        });
    });

    // Clean-up database
    after(done => {
        Thread.deleteMany({}).then( _ => {
            User.deleteMany({ }).then( _ => {
                done();
            })
        })
    });

    it('POST /comment/thread/:id - should create a new comment on thread', done => {
        chai.request(app).post(`/comment/thread/${testThread.id}`).send(testComment).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('id');
            res.body.should.have.property('threadId');
            assert.strictEqual(testThread.id, res.body.threadId);
            testComment.id = res.body.id;
            done();
        });
    });

    it('POST /comment/thread/:id - should not create a new comment on thread: Missing parameters', done => {
        chai.request(app).post(`/comment/thread/${testThread.id}`).send({ }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('POST /comment/:id - should create a new comment on comment', done => {
        chai.request(app).post(`/comment/${testComment.id}`).send(testComment).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('newCommentId');
            res.body.should.have.property('currentCommentId');
            assert.strictEqual(testComment.id, res.body.currentCommentId);
            done();
        });
    });

    it('POST /comment/:id/upvote - should upvote comment', done => {
        chai.request(app).post(`/comment/${testComment.id}/upvote`).send({
            username: testComment.author
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });

    it('POST /comment/:id/upvote - should not upvote comment, missing required parameter', done => {
        chai.request(app).post(`/comment/${testComment.id}/upvote`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('POST /comment/:id/downvote - should downvote comment', done => {
        chai.request(app).post(`/comment/${testComment.id}/downvote`).send({
            username: testComment.author
        }).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });

    it('POST /comment/:id/downvote - should not downvote comment, missing required parameter', done => {
        chai.request(app).post(`/comment/${testComment.id}/downvote`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(422);
            done();
        });
    });

    it('DELETE /comment/:id - should delete comment', done => {
        chai.request(app).delete(`/comment/${testComment.id}`).end((err, res) => {
            if (err) throw err;
            res.should.have.status(200);
            done();
        });
    });
});