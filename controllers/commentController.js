const Comment = require('../db/commentModel');
const UserController = require('./userController');
const ThreadController = require('./threadController');

async function createComment(content, author){
    if(!content || !author){
        console.error(`createComment: Missing paramaters`);
        return null;
    }
    
    const userId = await UserController.getUserIdByUsername(author);

    if(!userId) {
        console.error(`createComment: User not found`);
        return null;
    }

    const newComment = new Comment({ content: content, author: userId});

    await newComment.save(err => {
        if(err){
            console.log('createComment: MongoDB error.');
            console.error(err);
            return null;
        }
    });

    return newComment._id;
}

async function createCommentOnThread(req, res){
    const content = req.body.content;
    const author = req.body.author;
    const threadId = req.params.id;
    
    if(!content || !author || !threadId){
        console.log("createCommentOnThread: Missing parameters");
        return res.status(422).end();
    }

    const [comment, thread] = await Promise.all([
        createComment(content, author),
        ThreadController.getThreadById(threadId)
    ]);

    if(comment && thread){
        thread.comments.push(comment);
        thread.save( err => {
            if( err ){
                console.error(err);
                res.status(500).end();
            }
            else{
                console.log(`createCommentOnThread: Comment ${comment} toegevoegd aan thread ${threadId}`);
                res.status(200).json({id: comment, threadId: threadId}).end();
            }
        });
    }
    else{
        res.status(500).end();
        console.error('createThreadComment: Error');
    }
}

async function createCommentOnComment(req, res){
    const content = req.body.content;
    const author = req.body.author;
    const commentId = req.params.id;
    
    if(!content || !author || !commentId){
        console.log("createCommentOnComment: Missing parameters");
        return res.status(422).end();
    }

    const [newComment, comment] = await Promise.all([
        createComment(content, author),
        getCommentById(commentId)
    ]);

    if(newComment && comment){
        comment.comments.push(newComment);
        comment.save( err => {
            if( err ){
                console.error(err);
                res.status(500).end();
            }
            else{
                console.log(`createCommentOnComment: Comment ${newComment} toegevoegd aan de bestaande comment ${commentId}`);
                res.status(200).json({ newCommentId: newComment, currentCommentId: commentId}).end();
            }
        });
    }
    else{
        res.status(500).end();
        console.error('createCommentOnComment: Error');
    }
}

function deleteComment(req, res){
    const commentId = req.params.id;

    Comment.findByIdAndDelete(commentId)
        .then(_ => {
            console.log(`Comment ${commentId} has been succesfully deleted`);
            res.status(200).end();
        })
        .catch( err => {
            console.error(err);
            res.status(500).end();
        });
}

async function upvoteComment(req, res){
    const commentId = req.params.id;
    const username = req.body.username;

    if(!commentId || !username) return res.status(422).end();

    const user = await UserController.getUserIdByUsername(username);
    
    await Comment.updateMany({upvotes: user}, {$pull: { upvotes: user }}).catch(console.error);
    await Comment.updateMany({downvotes: user}, {$pull: { downvotes: user }}).catch(console.error);

    Comment.findByIdAndUpdate(commentId, {$push: { upvotes: user }}).then(_ => {
        console.log('upvoteComment: Success.');
        res.status(200).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

async function downvoteComment(req, res){
    const commentId = req.params.id;
    const username = req.body.username;

    if(!commentId || !username) return res.status(422).end();

    const user = await UserController.getUserIdByUsername(username);
    
    await Comment.updateMany({upvotes: user}, {$pull: { upvotes: user }}).catch(console.error);
    await Comment.updateMany({downvotes: user}, {$pull: { downvotes: user }}).catch(console.error);

    Comment.findByIdAndUpdate(commentId, {$push: { downvotes: user }}).then(_ => {
        console.log('downvoteComment: Success.');
        res.status(200).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

async function getCommentById(id){
    const comment = Comment.findById(id)
        .then(comment => {
            if(!comment) console.error(`getCommentById: Could not find comment with id ${id}`);
            return comment;
        })
        .catch(err => {
            console.error(`getCommentById: ${err}`);
            return null;
        }
    );
    return comment;
}

module.exports = {
    downvoteComment: downvoteComment,
    upvoteComment: upvoteComment,
    deleteComment: deleteComment,
    createCommentOnComment: createCommentOnComment,
    createCommentOnThread: createCommentOnThread
};