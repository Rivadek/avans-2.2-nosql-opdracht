const Thread = require('../db/threadModel');
const Comment = require('../db/commentModel');
const UserController = require('./userController');

async function createThread(req, res){
    const username = req.body.username;
    const title = req.body.title;
    const content = req.body.content;

    if(!username || !title || !content){
        console.log(`createThread: missing parameters.`);
        return res.status(422).end();
    }

    const userId = await UserController.getUserIdByUsername(username);

    if(userId === null){
        console.log(`createThread user does not exist in DB`);
        return res.status(500).end();
    }
    
    const newThread = new Thread({
        title: title,
        content: content,
        author: userId
    });

    newThread.save((error, thread) => {
        if(error){
            console.log('createThread: MongoDB error.');
            console.error(error);
            return res.status(500).end();
        }
        console.log('createThread: Success.');
        return res.status(200).json({id: thread._id, link: `/thread/${thread._id}`}).end();
    });
}

async function editThread(req, res){
    const objectId = req.params.objectId;
    const content = req.body.content;

    if(!objectId || !content){
        console.log('editThread: missing parameters.');
        return res.status(422).end();
    }

    Thread.findOneAndUpdate(objectId, {content: content}).then(_ => {
        console.log('editThread: Success.');
        res.status(200).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

function getThreads(req, res){
    Thread.aggregate([
        {$match: {}},
        {$lookup: {from: 'users', localField: 'author', foreignField: '_id', as: 'authorUser'}}, // join collections
        {$unwind: '$authorUser'}, // $lookup gives an array, make it an object.
        {$project: {_id: 1, title: 1, content: 1, username: '$authorUser.username'}} // projection
    ]).then(threads => {
        threads.forEach(thread => {
            thread.link = `/thread/${thread._id}`;
        });
        res.status(200).json(threads).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

function getThreadsSortByUpvotes(req, res){
    Thread.find({}).populate('author', 'username').sort({totalUpvotes: 1}).then(threads => {
        threads.forEach(thread => {
            thread.link = `/thread/${thread._id}`;
        });
        res.status(200).json(threads).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });;
}

async function getThread(req, res){
    const objectId = req.params.objectId;
    
    if(!objectId) return res.status(422).end();
    
    Thread.findOne({ _id: objectId }).then(thread => {
        res.status(200).json(thread).end();
    }).catch(err => {
        console.error(err);
        res.status(500).end();
    });
}

async function getThreadById(id){
    const thread = Thread.findById(id).then(thread => {
        if(!thread) console.error(`getThreadById: Could not find thread with id ${id}`);
        return thread;
    }).catch(err => {
        console.error(`getThreadById: ${err}`);
        return null;
    });
    return thread;
}

async function upvoteThread(req, res){
    const objectId = req.params.objectId;
    const username = req.body.username;

    if(!objectId || !username) return res.status(422).end();

    const user = await UserController.getUserIdByUsername(username);
    
    await Thread.updateMany({upvotes: user}, {$pull: { upvotes: user }}).catch(console.error);
    await Thread.updateMany({downvotes: user}, {$pull: { downvotes: user }}).catch(console.error);

    Thread.findOneAndUpdate(objectId, {$push: { upvotes: user }}).then(_ => {
        console.log('upvoteThread: Success.');
        res.status(200).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

async function downvoteThread(req, res){
    const objectId = req.params.objectId;
    const username = req.body.username;

    if(!objectId || !username) return res.status(422).end();

    const user = await UserController.getUserIdByUsername(username);
    
    await Thread.updateMany({upvotes: user}, {$pull: { upvotes: user }}).catch(console.error);
    await Thread.updateMany({downvotes: user}, {$pull: { downvotes: user }}).catch(console.error);

    Thread.findOneAndUpdate(objectId, {$push: { downvotes: user }}).then(_ => {
        console.log('downvoteThread: Success.');
        res.status(200).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

async function deleteThread(req, res){
    const objectId = req.params.objectId;

    if(!objectId) return res.status(422).end();

    const thread = await Thread.findById(objectId);
    Thread.findByIdAndDelete(objectId).then(_ => {
        thread.comments.forEach(comment => {
            Comment.findByIdAndDelete(comment).catch(console.error);
        });
        res.status(200).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

module.exports = {
    createThread: createThread,
    editThread: editThread,
    getThreads: getThreads,
    getThread: getThread,
    getThreadById: getThreadById,
    deleteThread: deleteThread,
    upvoteThread: upvoteThread,
    downvoteThread: downvoteThread,
    getThreadsSortByUpvotes: getThreadsSortByUpvotes,
};