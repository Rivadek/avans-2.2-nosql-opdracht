const User = require('../db/userModel');
const neo4j = require('../db/neo4j');

async function checkUserPassword(username, password){
    const user = await User.findOne({username: username, password: password}).then(user => {
        return user;
    }).catch(error => {
        console.error(error);
        return null;
    });
    return user !== null;
}

async function checkUsernameExists(username){
    const user = await User.findOne({username: username}).then(user => {
        return user;
    }).catch(error => {
        console.error(error);
        return null;
    });
    return user !== null;
}

function createUser(req, res){
    const username = req.body.username;
    const password = req.body.password;

    if(!username || !password){
        console.error(`createUser: Missing paramaters`);
        return res.status(422).end();
    }
    
    const newUser = new User({ username: username, password: password});

    newUser.save((err, user) => {
        if(err){
            console.error(err);
            res.status(500).end();
        }
        else{
            console.log(`createUser: New user ${username} has been created`);
            res.status(200).json({id: user._id}).end();
        }
    });
}

async function deleteUser(req, res){
    const username = req.body.username;
    const password = req.body.password;

    if(!username || !password){
        console.log('deleteUser: missing parameters');
        return res.status(422).end();
    }

    const isCorrectPassword = await checkUserPassword(username, password);

    if(isCorrectPassword){
        User.deleteOne({ username: username, password: password}).then(() =>{
            // Atomicity problem neo4j is run after user is deleted in Mongo
            const session = neo4j.session();
            const result = session.run('MATCH (u: User {username: $username}) DETACH DELETE u', {
                username: username
            });

            result.then(_ =>{
                console.log(`deleteUser: User ${username} is succesfully deleted`);
                res.status(200).end();
            });

            result.catch( err => {
                res.status(500).end();
                console.error(err);
            });
        }).catch(err => {
            console.error(err);
            res.status(500).end();
        });
    }
    else{
        console.log(`deleteUser: Incorrect password { username: ${username}, password: ${password} }`);
        res.status(401).end();
    }
}

async function getUserIdByUsername(username){
    const usernameExists = await checkUsernameExists(username);
    if(!usernameExists) return null;
    const user = await User.findOne({username: username}, {_id: 1}).then(user => {
        return user;
    }).catch(error => {
        console.error(error);
        return null;
    });
    return user._id;
}

async function editUserPassword(req, res){
    const username = req.body.username;
    const password = req.body.password;
    const newpassword = req.body.newpassword;

    if(!username || !password || !newpassword) return res.status(422).end();

    const isCorrectPassword = await checkUserPassword(username, password);
    if(!isCorrectPassword){
        console.log(`editUser: Incorrect password, {username: ${username}, password: ${password}}.`);
        return res.status(401).end();
    }
    User.findOneAndUpdate({username: username, password: password}, {password: newpassword}).then(_ => {
        console.log(`editUser: Successfully changed password, {username: ${username}, password: ${newpassword}}.`);
        res.status(200).end();
    }).catch(error => {
        console.error(error);
        res.status(500).end();
    });
}

async function addFriendship(req, res){
    const username1 = req.body.username1;
    const username2 = req.body.username2;

    if(!username1 || !username2) return res.status(422).end();

    const username1Exists = await checkUsernameExists(username1);
    const username2Exists = await checkUsernameExists(username2);

    if(!username1Exists || !username2Exists){
        console.log(`addFriendship: Error adding friends, one of the users doesn't exist. {username1: ${username1Exists} ${username1}, username2: ${username2Exists} ${username2}}.`);
        return res.status(404).end();
    }

    const session = neo4j.session();

    const result = session.run('MERGE (u1: User {username: $username1}) MERGE (u2: User{username: $username2}) MERGE (u1)<-[:IsFriends]->(u2)', {
        username1: username1,
        username2: username2
    });

    result.then(_ => {
        console.log(`addFriendship: Successfully added friends. {username1: ${username1}, username2: ${username2}}.`);
        session.close();
        res.status(200).end();
    });

    result.catch(error => {
        console.log(`addFriendship: Error adding friends. {username1: ${username1}, username2: ${username2}}.`);
        console.error(error);
        session.close();
        res.status(200).end();
    });
}

async function deleteFriendship(req, res){
    const username1 = req.body.username1;
    const username2 = req.body.username2;

    if(!username1 || !username2) return res.status(422).end();

    const username1Exists = await checkUsernameExists(username1);
    const username2Exists = await checkUsernameExists(username2);

    if(!username1Exists || !username2Exists){
        console.log(`deleteFriendship: Error deleting friends, one of the users doesn't exist. {username1: ${username1Exists} ${username1}, username2: ${username2Exists} ${username2}}.`);
        return res.status(404).end();
    }

    const session = neo4j.session();

    const result = session.run('MATCH (u1: User {username: $username1})-[r:IsFriends]-(u2: User {username: $username2}) DELETE r', {
        username1: username1,
        username2: username2
    });

    result.then(_ => {
        console.log(`deleteFriendship: Successfully deleted friends. {username1: ${username1}, username2: ${username2}}.`);
        session.close();
        res.status(200).end();
    });

    result.catch(error => {
        console.log(`deleteFriendship: Error deleting friends. {username1: ${username1}, username2: ${username2}}.`);
        console.error(error);
        session.close();
        res.status(200).end();
    });  
}

module.exports = {
    createUser: createUser,
    editUserPassword: editUserPassword,
    addFriendship: addFriendship,
    deleteFriendship: deleteFriendship,
    getUserIdByUsername: getUserIdByUsername,
    deleteUser: deleteUser,
};