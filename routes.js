const UserController = require('./controllers/userController');
const ThreadController = require('./controllers/threadController');
const CommentController = require('./controllers/commentController');

module.exports = (app) => {
    // User
    app.post('/user', UserController.createUser);
    app.patch('/user/password', UserController.editUserPassword);
    app.post('/user/friends', UserController.addFriendship);
    app.delete('/user', UserController.deleteUser);
    app.delete('/user/friends', UserController.deleteFriendship);
    // Thread
    app.post('/thread', ThreadController.createThread);
    app.patch('/thread/content/:objectId', ThreadController.editThread);
    app.get('/thread', ThreadController.getThreads);
    app.get('/thread/:objectId', ThreadController.getThread);
    app.get('/thread/sort/upvotes', ThreadController.getThreadsSortByUpvotes);
    app.delete('/thread/:objectId', ThreadController.deleteThread);
    app.post('/thread/:objectId/upvote', ThreadController.upvoteThread);
    app.post('/thread/:objectId/downvote', ThreadController.downvoteThread);
    // Comment
    app.post('/comment/thread/:id', CommentController.createCommentOnThread);
    app.post('/comment/:id', CommentController.createCommentOnComment);
    app.delete('/comment/:id', CommentController.deleteComment);
    app.post('/comment/:id/upvote', CommentController.upvoteComment);
    app.post('/comment/:id/downvote', CommentController.downvoteComment);
};
