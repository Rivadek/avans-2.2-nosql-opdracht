const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'comment' }],
    upvotes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
    downvotes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }]
},{
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true 
    }
});

schema.virtual('totalDownvotes').get(function () {
    return this.downvotes.length;
})

schema.virtual('totalUpvotes').get(function () {
    return this.upvotes.length;
})

function populateComments(next) {
    this.populate("comments");
    this.populate("author", "username");
    next();
  }
  
schema
    .pre('findOne', populateComments)
    .pre('find', populateComments);

schema.pre('remove', callback => {
    
});

module.exports = mongoose.model('thread', schema);