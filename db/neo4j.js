let neo4j = {
    driver: undefined,
    session() {
        return this.driver.session();
    }
};

module.exports = neo4j;