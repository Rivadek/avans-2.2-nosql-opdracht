const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'comment' }],
    upvotes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
    downvotes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }]
},{
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true 
    }
});

schema.virtual('totalDownvotes').get(function () {
    return this.downvotes.length;
})

schema.virtual('totalUpvotes').get(function () {
    return this.upvotes.length;
})

function populateComments(next) {
    this.populate("comments");
    this.populate("author", "username");
    next();
}
  
schema
    .pre('findOne', populateComments)
    .pre('find', populateComments);

schema.pre('remove', callback => {
    const Thread = mongoose.model('thread');
    thread.updateMany({}, {$pull: {'comments': {'_id': this._id}}})
        .then(() => {
            schema.updateMany({}, {$pull: {'comments' : {'_id' : this._id}}})
            .then(() => next())
        });
});

module.exports = mongoose.model('comment', schema);