const app = require('./app');
const port = process.env.PORT || (process.platform === 'win32' ? 80 : 3000);
app.listen(port, _ => {
    console.log(`Express app online at port ${port}.`);
});