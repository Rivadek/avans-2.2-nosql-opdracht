const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const neo4jdriver = require('neo4j-driver').v1;
const routes = require('./routes');
const neo4j = require('./db/neo4j');

// MongoDB
mongoose.Promise = global.Promise;
if(process.env.NODE_ENV === 'production'){
    mongoose.connect('mongodb://studdit:studdit123@ds117334.mlab.com:17334/avans', { // https://mongoosejs.com/docs/deprecations.html
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false
    }).then(_ => {
        console.log('MongoDB connected!');
    }).catch(console.error);
} else {
    mongoose.connect('mongodb://studdit:studdit123@ds119734.mlab.com:19734/avans-test', { // https://mongoosejs.com/docs/deprecations.html
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false
    }).then(_ => {
        console.log('MongoDB connected!');
    }).catch(console.error);
}

// Neo4J
if(process.env.NODE_ENV === 'production'){
    neo4j.driver = neo4jdriver.driver('bolt://hobby-ogckodfghkjagbkednkebfbl.dbs.graphenedb.com:24786', neo4jdriver.auth.basic('dev', 'b.HqYs5TVukk9O.05pkSXszZ9l4oHJ7'));
} else {
    neo4j.driver = neo4jdriver.driver('bolt://hobby-mmphmkniipmbgbkeadjeffbl.dbs.graphenedb.com:24786', neo4jdriver.auth.basic('dev', 'b.fVfuSFgs4jsF.6YhYp1tbg983NXZ2'));
}

// Express
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

routes(app);

process.on('exit', function() {
    neo4j.driver.close();
});

module.exports = app;